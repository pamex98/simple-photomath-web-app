# Simple Photomath

Make a photo of your math problem and this system will (hopefully) solve the problem for you. 

# How to use

There are two main use cases:
1) Webapp where you can upload/capture the image and get the final solution (python app.py or with docker)
2) Through the terminal where you can get the insight of all the intermediate steps (python simple_photomath.py [dir path with the images])

# Details

The solution consists of three main components:

1) Detector that detects characters - using OpenCV : bilateral filter -> grayscale -> adaptive threshold -> dilatation -> find contours -> filter contours -> crop contours 
2) Recognizer that recognizes each character (0123456789/x()+-) - preparing data for model ->  CNN (Resnet50v2 pretrained on image net) x2 ensemble
3) Solver that solves the final generated string - lexer -> parser -> math expression visitor


There is also one tiny component that arranges all detected characters by some order (between detector and recognizer). The system can solve both horizontal and vertical oriented input images. 

# Recognizer data


https://www.kaggle.com/patrikmesec/math-operators-and-digits - augmented version of https://www.kaggle.com/xainano/handwrittenmathsymbols (forward slash category is augmented with random rotation)


# Example 

![](pipeline_example.png)









