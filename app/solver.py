from __future__ import annotations
from enum import Enum
from dataclasses import dataclass
from abc import ABC, abstractmethod
from utils import WHITESPACE, DIVIDE, LEFT_BRACKET, RIGHT_BRACKET, NUMBERS, MULTIPLY, PLUS, MINUS


def solve(text, verbose=False):
    tokens = SimpleMathLexer(text).generate_tokens()
    parsed_tree = SimpleMathParser(tokens).parse()
    if verbose:
        print(f"parsed tree = {parsed_tree}")
    res = parsed_tree.accept(MathExpressionEvaluatorVisitor())
    return res


class TokenType(Enum):
    NUMBER = 0
    PLUS = 1
    MINUS = 2
    MULTIPLY = 3
    DIVIDE = 4
    LEFT_BRACKET = 5
    RIGHT_BRACKET = 6


@dataclass
class Token:
    type: TokenType
    value: any

    def __repr__(self):
        return self.type.name + (f":{self.value}" if self.value is not None else "")


class SimpleMathLexer:
    def __init__(self, text):
        self.text = iter(text)
        self.current_character = None
        self.step()

    def step(self):
        try:
            self.current_character = next(self.text)
        except StopIteration:
            self.current_character = None

    def generate_tokens(self):
        while self.current_character is not None:
            if self.current_character in WHITESPACE:
                self.step()
            elif self.current_character in NUMBERS:
                yield self.generate_number()
            elif self.current_character == PLUS:
                self.step()
                yield Token(TokenType.PLUS, None)
            elif self.current_character == MINUS:
                self.step()
                yield Token(TokenType.MINUS, None)
            elif self.current_character == MULTIPLY:
                self.step()
                yield Token(TokenType.MULTIPLY, None)
            elif self.current_character == DIVIDE:
                self.step()
                yield Token(TokenType.DIVIDE, None)
            elif self.current_character == LEFT_BRACKET:
                self.step()
                yield Token(TokenType.LEFT_BRACKET, None)
            elif self.current_character == RIGHT_BRACKET:
                self.step()
                yield Token(TokenType.RIGHT_BRACKET, None)
            else:
                raise Exception(
                    f"Character '{self.current_character}' not supported")

    def generate_number(self):
        num_str = self.current_character
        self.step()

        while self.current_character is not None and self.current_character in NUMBERS:
            num_str += self.current_character
            self.step()

        return Token(TokenType.NUMBER, float(num_str))


class Visitor(ABC):

    @abstractmethod
    def visit_number_node(self, number_node: NumberNode):
        pass

    @abstractmethod
    def visit_add_node(self, add_node: AddNode):
        pass

    @abstractmethod
    def visit_subtract_node(self, subtract_node: SubtractNode):
        pass

    @abstractmethod
    def visit_multiply_node(self, multiply_node: MultiplyNode):
        pass

    @abstractmethod
    def visit_divide_node(self, divide_node: DivideNode):
        pass

    @abstractmethod
    def visit_plus_node(self, plus_node: PlusNode):
        pass

    @abstractmethod
    def visit_minus_node(self, minus_node: MinusNode):
        pass


class Node(ABC):

    @abstractmethod
    def accept(self, visitor: Visitor):
        pass


@dataclass
class NumberNode(Node):
    value: float

    def __repr__(self):
        return f"{self.value}"

    def accept(self, visitor: Visitor):
        return visitor.visit_number_node(self)


@dataclass
class BinaryOperationNode(Node, ABC):
    left_node: Node
    right_node: Node


@dataclass
class UnaryOperationNode(Node, ABC):
    node: Node


class AddNode(BinaryOperationNode):

    def __repr__(self):
        return f"({self.left_node}+{self.right_node})"

    def accept(self, visitor: Visitor):
        return visitor.visit_add_node(self)


class SubtractNode(BinaryOperationNode):

    def __repr__(self):
        return f"({self.left_node}-{self.right_node})"

    def accept(self, visitor: Visitor):
        return visitor.visit_subtract_node(self)


class MultiplyNode(BinaryOperationNode):

    def __repr__(self):
        return f"({self.left_node}x{self.right_node})"

    def accept(self, visitor: Visitor):
        return visitor.visit_multiply_node(self)


class DivideNode(BinaryOperationNode):

    def __repr__(self):
        return f"({self.left_node}/{self.right_node})"

    def accept(self, visitor: Visitor):
        return visitor.visit_divide_node(self)


class PlusNode(UnaryOperationNode):

    def __repr__(self):
        return f"(+{self.node})"

    def accept(self, visitor: Visitor):
        return visitor.visit_plus_node(self)


class MinusNode(UnaryOperationNode):

    def __repr__(self):
        return f"(-{self.node})"

    def accept(self, visitor: Visitor):
        return visitor.visit_minus_node(self)


class SimpleMathParser:
    EXPRESSION_TOKENS = (TokenType.PLUS, TokenType.MINUS)
    TERM_TOKENS = (TokenType.MULTIPLY, TokenType.DIVIDE)

    def __init__(self, tokens):
        self.tokens = iter(tokens)
        self.current_token = None
        self.step()

    def step(self):
        try:
            self.current_token = next(self.tokens)
        except StopIteration:
            self.current_token = None

    def error(self):
        raise Exception("Invalid syntax")

    def parse(self):
        if self.current_token is None:
            return None
        res = self.expression()
        if self.current_token is not None:
            self.error()
        return res

    def expression(self):
        res = self.term()
        while self.current_token is not None and self.current_token.type in self.EXPRESSION_TOKENS:
            if self.current_token.type == TokenType.PLUS:
                self.step()
                res = AddNode(res, self.term())
            elif self.current_token.type == TokenType.MINUS:
                self.step()
                res = SubtractNode(res, self.term())
        return res

    def term(self):
        res = self.factor()
        while self.current_token is not None and self.current_token.type in self.TERM_TOKENS:
            if self.current_token.type == TokenType.MULTIPLY:
                self.step()
                res = MultiplyNode(res, self.factor())
            elif self.current_token.type == TokenType.DIVIDE:
                self.step()
                res = DivideNode(res, self.factor())
        return res

    def factor(self):
        current_token = self.current_token
        if current_token.type == TokenType.LEFT_BRACKET:
            self.step()
            res = self.expression()
            if self.current_token.type != TokenType.RIGHT_BRACKET:
                self.error()
            self.step()
            return res
        elif current_token.type == TokenType.NUMBER:
            self.step()
            return NumberNode(current_token.value)
        elif current_token.type == TokenType.PLUS:
            self.step()
            return PlusNode(self.factor())
        elif current_token.type == TokenType.MINUS:
            self.step()
            return MinusNode(self.factor())
        self.error()


class MathExpressionEvaluatorVisitor(Visitor):

    def visit_number_node(self, number_node: NumberNode):
        return number_node.value

    def visit_add_node(self, add_node: AddNode):
        return add_node.left_node.accept(self) + add_node.right_node.accept(self)

    def visit_subtract_node(self, subtract_node: SubtractNode):
        return subtract_node.left_node.accept(self) - subtract_node.right_node.accept(self)

    def visit_multiply_node(self, multiply_node: MultiplyNode):
        return multiply_node.left_node.accept(self) * multiply_node.right_node.accept(self)

    def visit_divide_node(self, divide_node: DivideNode):
        try:
            return divide_node.left_node.accept(self) / divide_node.right_node.accept(self)
        except Exception:
            raise Exception("Math error")

    def visit_plus_node(self, plus_node: PlusNode):
        return plus_node.node.accept(self)

    def visit_minus_node(self, minus_node: MinusNode):
        return - minus_node.node.accept(self)
