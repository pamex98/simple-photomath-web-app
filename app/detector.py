import cv2
import numpy as np

MIN_AREA_SIZE = 50


def detect(img, verbose=False):
    preprocessed_image = preprocess_image(img, verbose)
    boxes, cropped_imgs = _detection_impl(preprocessed_image, verbose)
    if verbose:
        draw_bounding_boxes(img, boxes)
    return boxes, cropped_imgs


def _detection_impl(img, verbose=False):
    contours, _ = cv2.findContours(
        img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if verbose:
        print(f"Num of contours = {len(contours)}")
    boxes = []
    cropped_imgs = []
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        if (w*h) < MIN_AREA_SIZE:
            continue
        box = [x, y, x + w, y + h]
        boxes.append(box)
        cropped_imgs.append(crop_image_contour(img, contour, box))
    if verbose:
        print(f"Num of boxes = {len(boxes)}")
    return boxes, cropped_imgs


def crop_image_contour(img, contour, box):
    mask = np.zeros_like(img)
    cv2.drawContours(mask, [contour], 0, 255, -1)
    out = np.zeros_like(img)
    out[mask == 255] = img[mask == 255]
    topx, topy, bottomx, bottomy = box
    out = out[topy:bottomy+1, topx:bottomx+1]
    return out

def draw_bounding_boxes(img, boxes):
    for box in boxes:
        x1, y1, x2, y2 = box
        cv2.rectangle(img, (x1, y1), (x2, y2), (0, 255, 0), 1)
    cv2.imshow("Detections", img)
    cv2.waitKey(0)


def preprocess_image(img, verbose=False):
    img = cv2.bilateralFilter(img, 9, 75, 75)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.adaptiveThreshold(
        img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 4)
    img = cv2.dilate(img, kernel=np.ones(
        (3, 3), dtype=np.uint8), iterations=1)
    if verbose:
        cv2.imshow("Preprocessed Image", img)
        cv2.waitKey(0)
    return img
