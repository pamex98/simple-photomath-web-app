from detector import detect
from recognizer import recognize
from solver import solve
from utils import arrange, read_img
import cv2
import os, sys


def compute(img, verbose=False):
    boxes, cropped_images = detect(img, verbose)
    cropped_images = arrange(boxes, cropped_images, verbose)
    symbols = recognize(cropped_images, verbose)
    if verbose:
        print(symbols)
    res = solve(symbols, verbose)
    return res


def main(argv):
    if len(argv) != 1:
        raise Exception("Test image directory argument not given!")
    TEST_DIR_PATH = argv[0]
    for i, file in enumerate(os.listdir(TEST_DIR_PATH)):
        img = read_img(os.path.join(TEST_DIR_PATH, file))
        cv2.imshow("Img", img)
        cv2.waitKey(0)
        try:
            print(f"res = {compute(img, True)}")
        except Exception:
            print("Cannot calculate")

if __name__ == '__main__':
    main(sys.argv[1:])
