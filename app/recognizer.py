from utils import LABEL_SYMBOL_DICT, resize_image
import numpy as np
import cv2
from skimage.morphology import thin
from recognizer_model import MODEL_IMG_HEIGHT, MODEL_IMG_WIDTH, getModel


def recognize(cropped_imgs, verbose=False):
    prepared_imgs = prepare_for_model(cropped_imgs, verbose)
    predictions = predict(prepared_imgs)
    return [LABEL_SYMBOL_DICT[index] for index in np.argmax(predictions, axis=1)]


def predict(imgs):
    model1_preds = model1.predict(imgs)
    model2_preds = model2.predict(imgs)
    return (model1_preds +  model2_preds) / 2


def prepare_for_model(images, verbose=False):
    prepared_imgs = []
    for img in images:
        processed = np.where(thin(img), 0, 255).astype(np.uint8)
        processed = resize_image(
            processed, (MODEL_IMG_WIDTH, MODEL_IMG_HEIGHT))
        if verbose:
            cv2.imshow("Final", processed)
            cv2.waitKey(0)
        prepared_imgs.append(np.stack((processed,)*3, axis=-1))
    return np.array(prepared_imgs)


MODEL1_CHECKPOINT_FILEPATH = "./models/model1/checkpoint_resnet50v2"
MODEL2_CHECKPOINT_FILEPATH = "./models/model2/checkpoint_resnet50v2"

model1 = getModel(MODEL1_CHECKPOINT_FILEPATH)
model2 = getModel(MODEL2_CHECKPOINT_FILEPATH)
