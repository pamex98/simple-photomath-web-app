from simple_photomath import compute
from utils import decode_img
from flask import Flask, request, render_template

app = Flask(__name__)


@app.route('/')
def upload_form():
    return render_template('index.html')


@app.route('/', methods=['POST'])
def upload_image():
    file = request.files['file']
    try:
        img = decode_img(file.read())
        res = f"Result = {compute(img)}"
    except Exception:
        res = "Cannot calculate :("
    return render_template('index.html', res=res)


if __name__ == '__main__':
    app.run()
