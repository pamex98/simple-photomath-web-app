import tensorflow as tf
import tensorflow_hub as hub
from utils import LABEL_SYMBOL_DICT
from tensorflow.keras import layers
import numpy as np
from sklearn.metrics import accuracy_score, classification_report

# const
BATCH_SIZE = 32
VALIDATION_SPLIT = 0.3
SEED = 999
MODEL_IMG_HEIGHT = 45
MODEL_IMG_WIDTH = 45
NUM_EPOCHS = 25
LR = 0.001
MOMENTUM = 0.9


def train(TRAIN_DIR, trainable_backbone=False, model_chkpt_file=None, save_model_filepath=None):
    train_ds, val_ds = prepare_dataset(TRAIN_DIR)
    model = getModel(model_chkpt_filepath=model_chkpt_file,
                     trainable_backbone=trainable_backbone)
    model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=LR, momentum=MOMENTUM),
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True), metrics=["accuracy"])
    model.fit(train_ds, validation_data=val_ds, epochs=NUM_EPOCHS,
              callbacks=get_model_checkpoint_callback(save_model_filepath))


def eval(model, eval_ds):
    y_true = []
    y_pred = []
    for image_batch, labels_batch in eval_ds:
        y_pred.extend(np.argmax(model.predict(image_batch), axis=1))
        y_true.extend(labels_batch.numpy().tolist())
    acc = accuracy_score(y_true, y_pred)
    report = classification_report(y_true, y_pred)
    print(f"acc = {acc}")
    print(f"report = {report}")

def getModel(model_chkpt_filepath=None, trainable_backbone=False):
    backbone = "https://tfhub.dev/google/imagenet/resnet_v2_50/feature_vector/5"
    feature_extractor_backbone = hub.KerasLayer(
        backbone, input_shape=(MODEL_IMG_WIDTH, MODEL_IMG_HEIGHT, 3), trainable=trainable_backbone)
    data_augmentation = tf.keras.Sequential([
        layers.RandomFlip("vertical"),
    ])
    normalization_layer = layers.Rescaling(1./255)
    m = tf.keras.Sequential([
        layers.InputLayer(input_shape=(MODEL_IMG_WIDTH, MODEL_IMG_HEIGHT, 3)),
        normalization_layer,
        data_augmentation,
        feature_extractor_backbone,
        tf.keras.layers.Dense(len(LABEL_SYMBOL_DICT))])

    if model_chkpt_filepath:
        m.load_weights(model_chkpt_filepath)
    return m


def get_model_checkpoint_callback(save_filepath):
    model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath=save_filepath,
        save_weights_only=True,
        monitor='val_accuracy',
        mode='max',
        save_best_only=True)
    return model_checkpoint_callback


def prepare_dataset(TRAIN_DIR):
    train_ds = tf.keras.utils.image_dataset_from_directory(
        TRAIN_DIR, validation_split=VALIDATION_SPLIT, subset="training", seed=SEED, image_size=(MODEL_IMG_WIDTH, MODEL_IMG_HEIGHT), batch_size=BATCH_SIZE)
    val_ds = tf.keras.utils.image_dataset_from_directory(
        TRAIN_DIR, validation_split=VALIDATION_SPLIT, subset="validation", seed=SEED, image_size=(MODEL_IMG_WIDTH, MODEL_IMG_HEIGHT), batch_size=BATCH_SIZE)
    AUTOTUNE = tf.data.AUTOTUNE
    train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
    val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)
    return train_ds, val_ds
