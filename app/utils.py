import numpy as np
import cv2
from scipy import ndimage

NUMBERS = "0123456789"
WHITESPACE = " \n\t"
PLUS = "+"
MINUS = "-"
MULTIPLY = "x"
DIVIDE = "/"
LEFT_BRACKET = "("
RIGHT_BRACKET = ")"
LABEL_SYMBOL_DICT = {0: LEFT_BRACKET, 1: RIGHT_BRACKET, 2: PLUS, 3: MINUS, 4: NUMBERS[0], 5: NUMBERS[1],
                     6: NUMBERS[2], 7: NUMBERS[3], 8: NUMBERS[4], 9: NUMBERS[5], 10: NUMBERS[6], 11: NUMBERS[7], 12: NUMBERS[8], 13: NUMBERS[9], 14: DIVIDE, 15: MULTIPLY}


def arrange(boxes, cropped_images, verbose=False):
    box_pos = np.array([[x0, y0] for x0, y0, x1, y1 in boxes])
    max = box_pos.max(axis=0)
    min = box_pos.min(axis=0)
    if verbose:
        print(f"max = {max}, min = {min}")
    order_by_index = np.argmax(max - min)
    if order_by_index != 0:
        # vertical, let's rotate backward
        modified_images = [ndimage.rotate(img, 90) for img in cropped_images]
    else:
        #horizontal, ok
        modified_images = cropped_images
    return [img for img, _ in sorted(zip(modified_images, boxes), key=lambda x: x[1][order_by_index])]


def resize_image(img, size=(28, 28)):

    h, w = img.shape[:2]
    c = img.shape[2] if len(img.shape) > 2 else 1

    if h == w:
        return cv2.resize(img, size, cv2.INTER_AREA)

    dif = h if h > w else w

    interpolation = cv2.INTER_AREA if dif > (
        size[0]+size[1])//2 else cv2.INTER_CUBIC

    x_pos = (dif - w)//2
    y_pos = (dif - h)//2

    if len(img.shape) == 2:
        mask = np.full((dif, dif), 255, dtype=img.dtype)
        mask[y_pos:y_pos+h, x_pos:x_pos+w] = img[:h, :w]
    else:
        mask = np.full((dif, dif, c), 255, dtype=img.dtype)
        mask[y_pos:y_pos+h, x_pos:x_pos+w, :] = img[:h, :w, :]

    return cv2.resize(mask, size, interpolation)


def read_img(file):
    return cv2.imread(file)


def decode_img(bytes):
    npimg = np.fromstring(bytes, np.uint8)
    img = cv2.imdecode(npimg, cv2.IMREAD_COLOR)
    return img
